package com.wether.javid.video;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.MediaController;
import android.widget.VideoView;

public class MainActivity extends AppCompatActivity {
    VideoView videoView;
    String url = "https://hw17.asset.aparat.com/aparat-video/c8cc0a1b232552823318ebb648378a979573365-144p__51415.mp4";
    BroadcastReceiver callReceiver;
    final SaveDurationClass save = new SaveDurationClass();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        videoView = findViewById(R.id.video);
        videoView.setMediaController(new MediaController(this));
        videoView.setVideoURI(Uri.parse(url));
        int duration = Integer.parseInt(save.getDuration());
        if(duration<=0){
            videoView.start();
        }else{
            videoView.seekTo(duration);
        }



        permissionReq();
        callReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (videoView.isPlaying()) {
                    videoView.pause();
                    String duration = videoView.getDuration()+ "";

                    save.setDuration(duration);
                    save.save();
                }
            }
        };



    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 111) {

        }
    }

    private void permissionReq() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.READ_PHONE_STATE}, 111);
        }

    }


}
