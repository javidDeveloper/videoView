package com.wether.javid.video;

import com.orm.SugarRecord;

/**
 * Created by Javid on 2/14/2018.
 */

public class SaveDurationClass extends SugarRecord<SaveDurationClass> {

    String duration ;

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }
}
